using Godot;
using System;
using System.Collections.Generic;

namespace Game
{
  public partial class Player : Entity
  {
	private const float JumpVelocity = -400.0f;
	private Dictionary<string, ICommand> _commandMap;
	public Vector2 velocity;

	public override void _Ready()
	{
	  _commandMap = new Dictionary<string, ICommand>
	  {
		{ "ui_left", new MoveLeftCommand() }
	  };
	}

	public override void _PhysicsProcess(double delta)
	{
	  velocity = Velocity;

	  foreach (var imputCommand in _commandMap)
	  {
		if (Input.IsActionPressed(imputCommand.Key))
		{
		  imputCommand.Value.Execute(this, delta);
		}
	  }

	  if (!IsOnFloor()) {

		velocity.Y += gravity * (float)delta;
	  }

	  if (Input.IsActionJustPressed("ui_up") && IsOnFloor())
		velocity.Y = JumpVelocity;

	  if (velocity.X > 1 || velocity.X < -1)
		ChangeAnimation("Run");
	  else
		ChangeAnimation("default");

// 	  Vector2 direction = Input.GetVector("ui_left", "ui_right", "ui_up", "ui_down");
// 	  if (direction != Vector2.Zero)
// 		velocity.X = direction.X * Speed;
// 	  else
// 		velocity.X = Mathf.MoveToward(Velocity.X, 0, Speed);
// 
	  Velocity = velocity;
	  MoveAndSlide();
	}

	public override void Die()
	{
	  	GD.Print("Player Died!");
	  	this.QueueFree();
	}

  }
}
