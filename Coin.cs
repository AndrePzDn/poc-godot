using Godot;

namespace Game
{
  public partial class Coin : Collectable
  {
	public override void OnCollect()
	{
	  GD.Print("Coin Collected!");
	}
	private void _on_body_entered(Node2D body)
	{
	  if (body is Player player)
	  {
		this.OnCollect();
		this.QueueFree();
	  }
	}

	public override void _Ready()
	{
	  base._Ready();
	  this.ChangeSprite("icon.svg");
	}
  }
}
