using Godot;

namespace Game
{
	public class MoveLeftCommand : ICommand
	{
		public void Execute(Player player, double delta)
		{
			player.velocity.X = Vector2.Left.X * player.Speed;
			player.velocity.X = Mathf.MoveToward(player.Velocity.X, 0, player.Speed);
		}
	}
}
