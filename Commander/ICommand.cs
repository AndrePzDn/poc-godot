namespace Game
{
    public interface ICommand
    {
        public void Execute(Player player, double delta);
    }
}
