using Godot;
using System;

namespace Game
{
	public abstract partial class Block : Area2D
	{
		[Export] private Area2D ActualNode { get; set; }
		[Export] private Sprite2D sprite { get; set; }
		[Export] private Texture2D InitialSprite { get; set; }
		private const string SpritesPath = "res://";


		public override void _Ready()
		{
			sprite.Texture = InitialSprite;
			ActualNode.Connect("body_entered", new Callable(this, "OnBodyEntered"));
		}

		public void ChangeSprite(Texture2D spriteTexture)
		{
			sprite.Texture = spriteTexture;
		}

		public virtual void OnBodyEntered(Node2D body)
		{
			GD.Print("Block hit!");
		}
	}
}
