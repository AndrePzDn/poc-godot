using Godot;

namespace Game
{
  public partial class BrickBlock : Block
  {

	[Export] private Texture2D UpdatedSprite { get; set; }
	public override void OnBodyEntered(Node2D body)
	{
	  if (body is Player player)
	  {
		GD.Print("Player hit the block!");
		this.ChangeSprite(UpdatedSprite);
	  }
	}
  }
}
