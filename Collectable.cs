using Godot;

namespace Game
{
	public abstract partial class Collectable : Area2D
	{
		[Export] public int Points { get; set; } = 0;
		[Export] private string SpriteNodeName { get; set; }
		private const string SpritesPath = "res://";

		private Sprite2D sprite;

		public abstract void OnCollect();

		public override void _Ready()
		{
			sprite = GetNode<Sprite2D>(SpriteNodeName);
		}

		public void ChangeSprite(string spriteName)
		{
			sprite.Texture = GD.Load<Texture2D>(SpritesPath + spriteName);
		}
	}
}
