using Godot;

namespace Game
{
  public abstract partial class Entity : CharacterBody2D
  {
	[Export] public float Speed { get; set; } = 0;
	public float gravity = ProjectSettings.GetSetting("physics/2d/default_gravity").AsSingle();
	[Export] private AnimatedSprite2D animatedSprite;

	public abstract void Die();

	public void ChangeAnimation(string animationName)
	{
	  animatedSprite.Play(animationName);
	}

  }
}
